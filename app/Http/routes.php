<?php


Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('welcome');
});


get('{slug}', 'FrontController@showPage');

Route::group(['prefix' => 'admin'], function () {

    get('dashboard', 'AdminController@dashboard');

    /* Pages */
    get('pages/destroy/{id}', 'PageController@destroy');
    resource('pages', 'PageController');

});

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');


/* Uploads */
Route::any('redactorUpload', function()
{
    $file = Request::file('file');
    $fileName = $file->getClientOriginalName();

    $file->move(public_path().'/uploads/', $fileName);
    return Response::json(array('filelink' => '/uploads/' . $fileName));
});