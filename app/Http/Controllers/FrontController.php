<?php

namespace Beats\Http\Controllers;

use Beats\Page;
use Illuminate\Http\Request;

use Beats\Http\Requests;
use Beats\Http\Controllers\Controller;

class FrontController extends Controller
{
    /**
     * Show Page(s)
     *
     * @param $slug
     * @return \Illuminate\View\View
     */
    public function showPage($slug)
    {
        $page = Page::where('slug', '=', $slug)->first();
        return view('page', compact('page'));
    }
}
