<?php

namespace Beats\Http\Controllers;

use Illuminate\Http\Request;

use Beats\Http\Requests;
use Beats\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function dashboard()
    {
        return view('dashboard');
    }

}
