<?php

namespace Beats\Http\Controllers;

use Beats\Http\Requests\CreatePageRequest;
use Beats\Page;
use Illuminate\Http\Request;
use Beats\Http\Requests;
use Beats\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $pages = Page::paginate(10);
        return view('pages.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(CreatePageRequest $request)
    {
        $str = array("&","'"," ");
        $rpl = array('-','-','-');

        $page = new Page;
        $page->name = $request->input('name');
        $page->seo_title = $request->input('seo_title');
        $page->seo_description = $request->input('seo_description');
        $page->content = $request->input('content');
        $page->status = $request->input('status');
        $page->slug = str_replace($str, $rpl, strtolower($request->input('slug')));
        $page->save();

        flash()->success('Success!','Page successfully created');
        return redirect('/admin/pages');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $page = Page::find($id);
        return view('pages.edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id, Request $request)
    {
        $str = array("&","'"," ");
        $rpl = array('-','-','-');

        $page = Page::find($id);
        $page->name = $request->input('name');
        $page->seo_title = $request->input('seo_title');
        $page->seo_description = $request->input('seo_description');
        $page->content = $request->input('content');
        $page->status = $request->input('status');
        $page->slug = str_replace($str, $rpl, strtolower($request->input('slug')));
        $page->update();

        flash()->success('Success!','Page successfully updated');
        return redirect('/admin/pages');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $page = Page::find($id);
        $page->delete();
        flash()->success('Success!','Page successfully removed');
        return redirect('admin/pages');
    }
}
