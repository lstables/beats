<div class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/"></a>
        </div>

        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <?php $pages = \Beats\Page::where('status', '=', 1)->where('show_in_nav', '=', 1)->get(); ?>
                @foreach($pages as $page)
                <li><a href="{!! $page->slug !!}">{!! ucwords($page->name) !!}</a></li>
                @endforeach
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="tel">Call: 01709 890968 or 07447 450990</li>
                <li class="tel">beatsworkindrumschool@gmail.com</li>
            </ul>
        </div><!--/.nav-collapse -->

    </div>
</div>

<div class="logo-section">
    <img src="/images/logo.png" class="logo img-responsive">
</div>