<div class="footer">
    <div class="col-xs-6 col-sm-4 copyright">
        &copy; {!! date('Y') !!} Beats Workin. <br />
        All rights reserved.<br />
        <small>
            Beats Workin Drum School,
            25 Fitzwilliam Street,
            Parkgate,
            Rawmarsh,
            Rotherham,
            S62 6EP
        </small>
    </div>
    <div class="col-xs-6 col-sm-4">
        <img src="/images/vic-firth-sm.png" alt="Vic Firth" width="100">&nbsp;&nbsp;
        <img src="/images/rsl-sm.png" alt="RockSchool Certification" width="100">
    </div>
    <div class="col-xs-6 col-sm-4">
        <div class="pull-right">
            <a href="https://www.facebook.com/BeatsWorkinDrumtuition" target="_blank"><i class="fa fa-facebook fa-3x"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="https://www.youtube.com/channel/UC_IrsmzAvOJaDMwsV7i6o7A" target="_blank"><i class="fa fa-youtube fa-3x"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="https://twitter.com/beatsworkinpdt" target="_blank"><i class="fa fa-twitter fa-3x"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="" target="_blank"><i class="fa fa-instagram fa-3x"></i></a>
        </div>
    </div>
    <br /><br />
</div>

<!-- javascript/jQuery -->
<script src="/js/jquery-1.11.1.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="/js/wow.js"></script>
<script> new WOW().init();</script>
<script src="/js/sweetalert.min.js"></script>
@include('flash')

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<script src="/js/html5shiv.min.js"></script>
<script src="/js/respond.min.js"></script>
<![endif]-->