<!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
     
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="img-responsive" src="/images/banner.png" alt="First slide">
        </div>

        <div class="item">
          <img class="img-responsive" src="/images/banner-2.png" alt="Second slide">
        </div>

      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <i class="fa fa-chevron-circle-left fa-2x"></i>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <i class="fa fa-chevron-circle-right fa-2x"></i>
        <span class="sr-only">Next</span>
      </a>
    </div><!-- /.carousel -->
<br /><br />


