<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Beats Working - Admin @yield('title')</title>
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- stylesheets -->
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/backend.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Google Font -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,200,600' rel='stylesheet' type='text/css'>
    <script>
        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN' : $('meta[name=_token]').attr('content') }
        });
    </script>

</head>
<body>

<!-- Static navbar -->
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin/dashboard">CMS</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="{{ Request::is('/admin/pages') ? 'active' : '' }}"><a href="/admin/pages">Pages</a></li>

                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li><a href="#">Action</a></li>--}}
                        {{--<li><a href="#">Another action</a></li>--}}
                        {{--<li><a href="#">Something else here</a></li>--}}
                        {{--<li role="separator" class="divider"></li>--}}
                        {{--<li class="dropdown-header">Nav header</li>--}}
                        {{--<li><a href="#">Separated link</a></li>--}}
                        {{--<li><a href="#">One more separated link</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            </ul>
            @if (Auth::check())
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/auth/logout">Logout</a></li>
            </ul>
            @endif
        </div><!--/.nav-collapse -->
    </div>
</nav>


    <div class="container">

        @yield('content')

    </div>

    <!-- javascript/jQuery -->
    <script src="/js/jquery-1.11.1.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/wow.js"></script>
    <script> new WOW().init();</script>
    <script src="/js/sweetalert.min.js"></script>
    @include('flash')

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="/js/html5shiv.min.js"></script>
    <script src="/js/respond.min.js"></script>
    <![endif]-->

    <!-- Redactor Plugins -->
    <link rel="stylesheet" href="/redactor/redactor.css" />
    <script src="/redactor/redactor.min.js"></script>
    <script src="/redactor/table.js"></script>
    <script src="/redactor/fontcolor.js"></script>
    <script src="/redactor/fontsize.js"></script>
    <script src="/redactor/video.js"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            // All textareas become redactor wysiwyg areas.
            $('textarea').redactor({
                focus: true,
                imageUpload: '/redactorUpload?_token=' + '{{ csrf_token() }}',
                fileUpload: '/redactorUpload',
                minHeight: 450,
                plugins: ['table', 'fontcolor', 'fontsize', 'video'],
                linebreaks: true,
            });
        });
    </script>

</body>
</html>