<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Beats Working - @if (! empty($page->seo_title)) {!! $page->seo_title !!} @endif</title>
    <meta name="description" content="@if (! empty($page->seo_description)) {!! $page->seo_description !!} @endif">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- stylesheets -->
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/frontend.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Google Font -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,200,600' rel='stylesheet' type='text/css'>

</head>
<body>
@include('partials/nav')

@include('partials/slider')
<div class="container content">

    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-8 col-lg-12">
            <h1>@if (! empty($page->seo_title)) {!! $page->seo_title !!} @endif</h1>
            <p>
                {!! $page->content !!}
            </p>
        </div>
    </div>
    <script type="text/javascript" src="http://www.freeindex.co.uk/widgets/fiwidget.asp?lid=571083%26tit%3D%26wid%3D200%26agt%3D1%26rak%3D0%26dis%3D0%26wri%3D0%26nrv%3D0%26rts%3DS%26theme%3Dlight"></script>

    <hr />
    @include('partials/footer')
</div>
</body>
</html>
