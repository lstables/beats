<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Beats Working - Professional Drum Tuition</title>
    <?php $page = \Beats\Page::where('slug', '=', 'home')->first(); ?>
    @if (Request::is('/') || Request::is('home')) <meta name="description" content="{!! $page->seo_description !!}"> @endif
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- stylesheets -->
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/frontend.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/sweetalert.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Google Font -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,200,600' rel='stylesheet' type='text/css'>
    <!-- Ajax setup, CSRF Token -->
    <script>
        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN' : $('meta[name=_token]').attr('content') }
        });
    </script>
</head>
<body>
@include('partials/nav')

@include('partials/slider')
<div class="container content">
    <div class="marketing">
        
        <div class="row">
            <div class="col-lg-4">
                <a href="/grades" class="no-underline"><img class="img-circle" src="/images/rsl.png" alt="Rockschool Grades" width="140" height="140">
                <h2>Grades</h2></a>
                <p>A Rockschool Grade is a true gauge of your playing and musical ability.
                    At Beats Workin you'll learn what is tailored to fit real-world music situations, and your exams are based on performance –
                    passing our Grades proves you can perform your music to a high standard when it matters the most.</p>
                <img src="/images/free_first_lesson.jpg" width="130">
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <a href="/tuition" class="no-underline"><img class="img-circle" src="/images/drummer.jpg" alt="Beats Workin professional drum tuition" width="140" height="140">
                <h2>Tuition</h2></a>
                <p>
                Beats Workin offers its students a full live experience, you can play along to your favorite band with full P.A backing to give you that head start and to build confidence before that big gig!
                </p>
                <img src="/images/free_lesson.jpg" width="130">
            </div><!-- /.col-lg-4 -->
            <div class="col-lg-4">
                <a href="/guides" class="no-underline"><img class="img-circle" src="/images/vic-firth.png" alt="Vic Firth as Private Drum Teachers" width="140" height="140">
                <h2>Guides</h2></a>
                <p>We are registered with <a href="http://www.vicfirth.com" target="_blank">Vic Firth</a> as Private Drum Teachers and keep up to date with
                    modern teaching methods so whether you are a complete beginner and have never picked
                    up a pair of sticks or you are wanting to get through your Rock School Grades or beyond
                    we can help.</p>
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
        <hr />
    </div>
<div class="row">
@if (Request::is('/') || Request::is('home'))
    <?php $page = \Beats\Page::where('slug', '=', 'home')->first(); ?>
    <div class="col-xs-12 col-sm-6 col-md-8">
        <h1>Welcome to Beats Workin</h1>
        <p>
           {!! $page->content !!}
        </p>
    </div>
    <div class="col-xs-6 col-md-4">
        <iframe width="367" height="315" src="https://www.youtube.com/embed/dAKQ1yj4hi0" frameborder="0" allowfullscreen></iframe>
    </div>
</div>
<script type="text/javascript" src="http://www.freeindex.co.uk/widgets/fiwidget.asp?lid=571083%26tit%3D%26wid%3D200%26agt%3D1%26rak%3D0%26dis%3D0%26wri%3D0%26nrv%3D0%26rts%3DS%26theme%3Dlight"></script>
@else
    @yield('content')
@endif

<hr />
@include('partials/footer')

</div>
</body>
</html>
