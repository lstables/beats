@extends('admin.layout')
@section('title')
    Dashboard
@stop
@section('content')

    <div class="panel panel-default">
      <div class="panel-heading">Hi {!! Auth::user()->name !!}</div>
      <div class="panel-body">
            <p>To get started, please select from the above.</p>
      </div>
    </div>

@stop