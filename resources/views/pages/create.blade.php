@extends('admin.layout')
@section('title')
    Create Page
@stop
@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            Create Page
        </div>
        <div class="panel-body">

            {!! Form::open(['url' => 'admin/pages', 'class' => 'form-horizontal']) !!}

                {!! csrf_field() !!}

                <div class="form-group  {!! $errors->has('name') ? 'has-error' : '' !!}">
                    <div class="col-sm-2">Page Name</div>
                    <div class="col-lg-5">
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="col-lg-4">{!! $errors->first('name', '<span class="help-block">:message</span>') !!}</div>
                </div>

                <div class="form-group  {!! $errors->has('seo_title') ? 'has-error' : '' !!}">
                    <div class="col-sm-2">SEO Title</div>
                    <div class="col-lg-5">
                        <input type="text" name="seo_title" class="form-control" value="">
                    </div>
                    <div class="col-lg-4">{!! $errors->first('seo_title', '<span class="help-block">:message</span>') !!}</div>
                </div>

                <div class="form-group  {!! $errors->has('seo_description') ? 'has-error' : '' !!}">
                    <div class="col-sm-2">SEO Description</div>
                    <div class="col-lg-5">
                        <input type="text" name="seo_description" class="form-control" value="">
                    </div>
                    <div class="col-lg-4">{!! $errors->first('seo_description', '<span class="help-block">:message</span>') !!}</div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2">Content</div>
                    <div class="col-lg-10">
                        <textarea name="content" rows="12" class="form-control"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2">Slug/URL</div>
                    <div class="col-lg-5">
                        <input type="text" name="slug" class="form-control" value="">
                        <div class="help-block">Example: <code>my new page</code> will result in <code>my-new-page</code> when saved.</div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2">Status</div>
                    <div class="col-lg-5">
                        <select name="status" class="form-control">
                            <option value="1">Active</option>
                            <option value="0">Inactive</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2">Show in Navigation</div>
                    <div class="col-lg-5">
                        <select name="show_in_nav" class="form-control">
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                        <div class="help-block">Show this page in the main site navigation</div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-3">
                        <button class="btn btn-success">Save</button>
                    </div>
                </div>

            {!! Form::close() !!}

        </div>
    </div>

@stop