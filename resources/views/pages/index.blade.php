@extends('admin.layout')
@section('title')
    Pages
@stop
@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            Pages @if (count($pages)) ({!! $pages->total() !!}) @endif
            <div class="pull-right">
                <a href="/admin/pages/create" class="btn btn-sm btn-primary">Create Page</a>
            </div>
        </div>
        <div class="panel-body">
        @if (count($pages))
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Page Name</th>
                            <th>Slug/URL</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach($pages as $page)
                        <tr>
                            <td>{!! $page->name !!}</td>
                            <td>{!! $page->slug !!}</td>
                            <td>@if ($page->status == 1) <span class="label label-success">Active</span> @else <span class="label label-danger">Inactive</span> @endif</td>
                            <td>
                                <a href="/admin/pages/{!! $page->id !!}/edit" class="btn btn-sm btn-warning">Edit</a>
                                <a href="/admin/pages/destroy/{!! $page->id !!}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to remove this?')">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {!! $pages->render() !!}
            </div>
        @else
            <p>No pages.</p>
        @endif
        </div>
    </div>

@stop