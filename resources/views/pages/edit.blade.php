@extends('admin.layout')
@section('title')
    Edit Page
@stop
@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            Editing Page
        </div>
        <div class="panel-body">
            <div class="table-responsive">
                {!! Form::open(['method' => 'PUT', 'url' => '/admin/pages/' . $page->id, 'class' => 'form-horizontal']) !!}
                {!! csrf_field() !!}
                <div class="form-group">
                    <div class="col-sm-2">Page Name</div>
                    <div class="col-lg-5">
                        <input type="text" name="name" class="form-control" value="{!! $page->name !!}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2">SEO Title</div>
                    <div class="col-lg-5">
                        <input type="text" name="seo_title" class="form-control" value="{!! $page->seo_title !!}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2">SEO Description</div>
                    <div class="col-lg-5">
                        <input type="text" name="seo_description" class="form-control" value="{!! $page-> seo_description !!}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2">Content</div>
                    <div class="col-lg-10">
                        <textarea name="content" rows="12" class="form-control">{!! $page->content !!}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2">Slug/URL</div>
                    <div class="col-lg-5">
                        @if($page->slug == '/')
                            <input type="text" name="slug" class="form-control" value="/" readonly>
                        @else
                            <input type="text" name="slug" class="form-control" value="{!! $page->slug !!}" readonly>
                            <div class="help-block">Example: <code>my new page</code> will result in <code>my-new-page</code> when saved.</div>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-2">Status</div>
                    <div class="col-lg-5">
                        {!! Form::select('status', ['Inactive','Active'], $page->status, array('class' => 'form-control')) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-3">
                        <button class="btn btn-success">Save</button>
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>

@endsection